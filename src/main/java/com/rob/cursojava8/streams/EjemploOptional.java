/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rob.cursojava8.streams;

import com.rob.cursojava8.modelo.Estudiante;
import java.math.BigDecimal;
import java.nio.file.Path;
import java.util.Optional;

/**
 *
 * @author Rob
 */
public class EjemploOptional {

    public static void main(String[] args) {
        Estudiante eA = new Estudiante(1, "Juan", "Perez", 36, null, BigDecimal.valueOf(256.60));
        Optional<Estudiante> estA = Optional.of(eA);
        Estudiante eB = null;
        Optional<Estudiante> estB = Optional.ofNullable(eB);

        System.out.println(estA);
        System.out.println(estB);
        
        estB.ifPresent(e -> System.out.println(e));
                
    }

}
