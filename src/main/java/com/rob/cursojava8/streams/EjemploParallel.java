/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rob.cursojava8.streams;

import com.rob.cursojava8.modelo.Estudiante;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

/**
 *
 * @author Rob
 */
public class EjemploParallel {

    public static void imprimirNumeros(int min, int max) {
        System.out.println("Normal");
        IntStream rangoNumeros = IntStream.range(min, max);
        long tiempoInicial = System.currentTimeMillis();
        System.out.println("Conteo: " + rangoNumeros.average());    
        long tiempoFinal = System.currentTimeMillis() - tiempoInicial;
        System.out.println("Tiempo de duración: " + tiempoFinal + " ms");

        System.out.println("\n\n*********************************");
        System.out.println("Parallel");
        IntStream rangoNumerosParallel = IntStream.range(min, max);
        long tiempoInicialParallel = System.currentTimeMillis();
        System.out.println("Conteo: " + rangoNumerosParallel.parallel().average());    
        long tiempoFinalParallel = System.currentTimeMillis() - tiempoInicialParallel;
        System.out.println("Tiempo de duración: " + tiempoFinalParallel + " ms");

    }

    public static void imprimirObjetos(List<Estudiante> estudiantes) {
        System.out.println("Normal");
        long tiempoInicial = System.currentTimeMillis();
        estudiantes.stream().forEach(e -> System.out.println("Hilo: " + Thread.currentThread() + e));
        long tiempoFinal = System.currentTimeMillis() - tiempoInicial;
        System.out.println("Tiempo de duración: " + tiempoFinal + " ms");

        System.out.println("\n\n*********************************");
        System.out.println("Parallel");
        long tiempoInicialParallel = System.currentTimeMillis();
        estudiantes.parallelStream().forEach(i -> System.out.println("Hilo: " + Thread.currentThread() + i));
        long tiempoFinalParallel = System.currentTimeMillis() - tiempoInicialParallel;
        System.out.println("Tiempo de duración: " + tiempoFinalParallel + " ms");

    }

    public static void main(String[] args) {
        System.out.println("Imprimir números en una secuencia utlizando los modos Stream y Parallel");
        EjemploParallel.imprimirNumeros(1, 1000000000);

        System.out.println("\nImprimir estudiantes utlizando los modos Stream y Parallel");
        List<Estudiante> estudiantes = Arrays.asList(new Estudiante(1, "Juan", "Perez", 19, null),
                new Estudiante(2, null, "Navaja", 60, null),
                new Estudiante(3, "Agus", "Lagos", 7, null),
                new Estudiante(4, "Pepa", "Pig", 8, null),
                new Estudiante(5, "Johnny", "Bravo", 30, null),
                new Estudiante(6, "William", "Wallace", 36, null));
        EjemploParallel.imprimirObjetos(estudiantes);
    }
}
