/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rob.cursojava8.streams;

import com.rob.cursojava8.modelo.Estudiante;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Rob
 */
public class EjemploMatching {

    public static void main(String[] args) {
        List<Estudiante> estudiantes = Arrays.asList(new Estudiante(1, "Juan", "Perez", 19, null, new BigDecimal(363.65)),
                new Estudiante(1, null, "Navaja", 60, null, new BigDecimal(263.50)),
                new Estudiante(3, "Agus", "Lagos", 7, null, new BigDecimal(303.25)),
                new Estudiante(4, "Pepa", "Pig", 8, null, new BigDecimal(215.70)),
                new Estudiante(5, "Johnny", "Bravo", 30, null, new BigDecimal(455.10)),
                new Estudiante(6, "William", "Wallace", 36, null, new BigDecimal(220.70)),
                new Estudiante(7, "Juan", "Wallace", 10, null, new BigDecimal(136.50)));

        Boolean cumpleCondicion = estudiantes.stream().allMatch(e -> e.getPension().doubleValue() > 200);

        System.out.println("Operaciones reduce con combinator");
        List<Integer> arregloNumeros = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
        int valor = arregloNumeros.stream()
                .reduce(3, (n1, n2) -> n1 - n2, (n1, n2) -> n1 - n2)
                .intValue();
        System.out.println("Valor: " + valor);

        BigDecimal restultado = estudiantes.parallelStream()
                .map(Estudiante::getPension)
                .reduce(BigDecimal.ZERO, BigDecimal::add, BigDecimal::add);

        System.out.println("Resultado: " + restultado.setScale(2, RoundingMode.HALF_UP));

    }
}
