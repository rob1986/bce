/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rob.cursojava8.streams;

import com.rob.cursojava8.modelo.Estudiante;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Comparator;
import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author Rob
 */
public class EjemploMapa {

    public static void main(String[] args) {
        List<Estudiante> estudiantes = Arrays.asList(new Estudiante(1, "Juan", "Perez", 19, null, new BigDecimal(363.65)),
                new Estudiante(1, null, "Navaja", 60, null, new BigDecimal(263.50)),
                new Estudiante(3, "Agus", "Lagos", 7, null, new BigDecimal(303.25)),
                new Estudiante(4, "Pepa", "Pig", 8, null, new BigDecimal(215.70)),
                new Estudiante(5, "Johnny", "Bravo", 30, null, new BigDecimal(455.10)),
                new Estudiante(6, "William", "Wallace", 36, null, new BigDecimal(220.70)),
                new Estudiante(7, "Juan", "Wallace", 10, null, new BigDecimal(136.50)));

        System.out.println("\n\b Recuperar apellidos de estudiantes");
        estudiantes.stream()
                .sorted(Comparator.comparing(Estudiante::getApellido).reversed())
                .map(Estudiante::getApellido)
                .distinct()
                .forEach(System.out::println);

        System.out.println("\n\nPromedio en pensiones");
        Double promedio = estudiantes.stream()
                .map(Estudiante::getPension)
                .mapToDouble(d -> Double.parseDouble(d.toString()))
                .average().getAsDouble();
        System.out.println("Promedio en pensiones: " + promedio);

        DoubleSummaryStatistics ds = estudiantes.stream().map(Estudiante::getPension).collect(Collectors.summarizingDouble(d -> d.doubleValue()));
        System.out.println("DS: " + ds);

    }

}
