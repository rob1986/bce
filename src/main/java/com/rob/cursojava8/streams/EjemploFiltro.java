/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rob.cursojava8.streams;

import com.rob.cursojava8.modelo.Estudiante;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author Rob
 */
public class EjemploFiltro {

    public static List<Estudiante> filtrarPorEdad(List<Estudiante> estudiantes) {
        return estudiantes.parallelStream().filter((Estudiante e) -> e.getEdad() > 18).collect(Collectors.toList());
    }

    public static void main(String[] args) {
        List<Estudiante> estudiantes = Arrays.asList(new Estudiante(1, "Juan", "Perez", 19, null),
                new Estudiante(1, null, "Navaja", 60, null),
                new Estudiante(3, "Agus", "Lagos", 7, null),
                new Estudiante(4, "Pepa", "Pig", 8, null),
                new Estudiante(5, "Johnny", "Bravo", 30, null),
                new Estudiante(6, "William", "Wallace", 36, null));
        filtrarPorEdad(estudiantes).forEach(System.out::println);

        estudiantes.stream().distinct().forEach(System.out::println);

        estudiantes.stream().sorted(Comparator.comparing(Estudiante::getNombre).reversed()).forEach(System.out::println);
    }
}
