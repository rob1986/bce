/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rob.cursojava8.streams;

import java.util.Arrays;
import java.util.stream.Stream;

/**
 *
 * @author Rob
 */
public class EjemploFlatMap {

    public static void main(String[] args) {
        String[][] nombres = new String[][]{{"Marco", "Luis"}, {"Vilma", "Pedro"}, {"Mateo", "Fabian"}};

        System.out.println("Imprimir los nombres que terminan en o");
        Stream<String[]> streamNombres = Arrays.stream(nombres);
        streamNombres.filter(n -> n[0].toString().endsWith("o") || n[1].toString().endsWith("o")).forEach(System.out::println);

        System.out.println("Imprimir los nombres que terminan en o, con flatMap");
        Stream<String> streamNombresII = Arrays.stream(nombres).flatMap(n -> Arrays.stream(n));
        streamNombresII.filter(n -> n.endsWith("o")).forEach(System.out::println);
    }
}
