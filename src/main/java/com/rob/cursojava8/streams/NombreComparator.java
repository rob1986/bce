/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rob.cursojava8.streams;

import com.rob.cursojava8.modelo.Estudiante;
import java.util.Comparator;

/**
 *
 * @author Rob
 */
public class NombreComparator implements Comparator<Estudiante> {
    
    @Override
    public int compare(Estudiante o1, Estudiante o2) {
        return o1.getNombre().compareTo(o2.getNombre());
    }
    
}
