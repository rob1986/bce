/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rob.cursojava8.streams;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

/**
 *
 * @author Rob
 */
public class EjemploFile {

    public static void main(String[] args) {

        String nombreArchivo = "Archivo.txt";

        try ( Stream<String> streamFile = Files.lines(Paths.get(nombreArchivo))) {
            
        } catch (Exception ex) {

        }
    }
}
