/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rob.cursojava8.behavior.ejemplo;

import com.rob.cursojava8.modelo.Estudiante;
import java.util.Arrays;
import java.util.List;
import java.util.function.BinaryOperator;
import java.util.function.UnaryOperator;

/**
 *
 * @author Rob
 */
public class EjemploBinaryOperator {

    public static void main(String[] args) {
        List<Integer> numeros = Arrays.asList(4, 5, 6, 7, 8, 8);
        BinaryOperator<Integer> suma = (val1, val2) -> {
            int c = 0;
            c = val1 + val2;
            return c;
        };
        System.out.println("Suma: " + suma.apply(1, 2));

        UnaryOperator<Estudiante> empUni = (Estudiante est) -> {
            est.setEdad(23);
            return est;
        };

        Estudiante estudiante = new Estudiante(1, "Juan", "Peres", 30, null);
        System.out.println("Estudiante antes: " + estudiante);
        empUni.apply(estudiante);
        System.out.println("Estudiante despues: " + estudiante);

    }
}
