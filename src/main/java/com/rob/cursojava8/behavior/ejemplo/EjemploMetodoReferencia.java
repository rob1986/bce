/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rob.cursojava8.behavior.ejemplo;

import com.rob.cursojava8.modelo.Estudiante;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author Rob
 */
public class EjemploMetodoReferencia {

    public static void main(String[] args) {
        List<Estudiante> estudiantes = Arrays.asList(new Estudiante(1, "Juan", "Perez", 19, null),
                new Estudiante(1, "Pedro", "Navaja", 60, null),
                new Estudiante(1, "Agus", "Lagos", 7, null),
                new Estudiante(1, "Pepa", "Pig", 8, null),
                new Estudiante(1, "Johnny", "Bravo", 30, null),
                new Estudiante(1, "William", "Wallace", 36, null));

        estudiantes.stream().sorted(Comparator.comparing(Estudiante::getApellido)).forEach(System.out::println);
    }

}
