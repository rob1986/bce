/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rob.cursojava8.behavior.ejemplo;

import com.rob.cursojava8.behavior.ControladorEstudiante;
import com.rob.cursojava8.behavior.EdadEstudiante;
import com.rob.cursojava8.behavior.EdadNombreEstudiante;
import com.rob.cursojava8.behavior.IOperacion;
import com.rob.cursojava8.behavior.PredicadoEstudiante;
import com.rob.cursojava8.modelo.Estudiante;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 *
 * @author Rob
 */
public class EjemploBehavior {

    public static List<Estudiante> filtrar(List<Estudiante> estudiantes, PredicadoEstudiante predicadoEstudiante) {
        List<Estudiante> resultado = new ArrayList<>();
        for (Estudiante estudiante : estudiantes) {
            if (predicadoEstudiante.filtrar(estudiante)) {
                resultado.add(estudiante);
            }
        }
        return resultado;
    }

    public static List<Estudiante> filtrarLambda(List<Estudiante> estudiantes, PredicadoEstudiante predicadoEstudiante) {
        return estudiantes.stream().filter((Estudiante e) -> predicadoEstudiante.filtrar(e)).collect(Collectors.toList());
    }

    public static Integer sumatoria(List<Integer> enteros, IOperacion o) {
        Integer total = 0;
        for (Integer i : enteros) {
            total = o.operacion(total, i);
        }
        return total;
    }

    public static void main(String[] args) {
        List<Estudiante> estudiantes = Arrays.asList(new Estudiante(1, "Juan", "Perez", 19, null),
                new Estudiante(1, "Pedro", "Navaja", 60, null),
                new Estudiante(1, "Agus", "Lagos", 7, null),
                new Estudiante(1, "Pepa", "Pig", 8, null),
                new Estudiante(1, "Johnny", "Bravo", 30, null),
                new Estudiante(1, "William", "Wallace", 36, null));

        Consumer<Estudiante> funcionImpresion = (Estudiante e) -> System.out.println(e);

        //Tradicional
        System.out.println("\n****FILTRADO TRADICIONAL****");
        System.out.println("\n****Por edad (mayores 18)****");
        ControladorEstudiante.filtrarPorEdad(estudiantes).forEach(funcionImpresion);
        System.out.println("\n****Por nombre y edad (mayores 18 y empieza con A)****");
        ControladorEstudiante.filtrarPorEdadNombre(estudiantes).forEach(funcionImpresion);

        //Strategy Pattern
        System.out.println("\n****FILTRADO STRATEGY PATTERN****");
        System.out.println("\n****Por edad (mayores 18)****");
        PredicadoEstudiante predicadoEdad = new EdadEstudiante();
        filtrar(estudiantes, predicadoEdad).forEach(funcionImpresion);
        System.out.println("\n****Por nombre y edad (mayores 18 y empieza con A)****");
        PredicadoEstudiante predicadoEdadNombre = new EdadNombreEstudiante();
        filtrarLambda(estudiantes, predicadoEdadNombre).forEach(funcionImpresion);

        //Ordenamiento
        System.out.println("\n****LAMBDA ORDENAMIENTO****");
        System.out.println("\n****Estudiantes ordenados por edad****");
        estudiantes.stream().sorted((Estudiante a, Estudiante b) -> a.getEdad().compareTo(b.getEdad())).forEach(funcionImpresion);

        //Sumatoria
        IOperacion funcionSuma = (a, b) -> a + b;
        List<Integer> enteros = Arrays.asList(1, 2, 3, 4, 5);
        System.out.println("Suma: " + sumatoria(enteros, funcionSuma));

    }
}
