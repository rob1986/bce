/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rob.cursojava8.behavior.ejemplo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 *
 * @author Rob
 */
public class EjemploPredicado {
    
    public static <T> List<T> filtrar(List<T> lista, Predicate<T> predicado) {
        return lista.stream().filter(predicado).collect(Collectors.toList());
    }
    
    public static void main(String[] args) {
        List<String> archivos = Arrays.asList("trabajo.txt", "pagos.jpg", "deber.rtf");
        Predicate<String> predicadoTipoTxt = (String s) -> s.endsWith(".txt");
        filtrar(archivos, predicadoTipoTxt).forEach(System.out::println);
    }
}
