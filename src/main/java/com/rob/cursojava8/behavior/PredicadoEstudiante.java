/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rob.cursojava8.behavior;

import com.rob.cursojava8.modelo.Estudiante;

/**
 *
 * @author Rob
 */
@FunctionalInterface
public interface PredicadoEstudiante {

    public Boolean filtrar(Estudiante estudiante);
}
