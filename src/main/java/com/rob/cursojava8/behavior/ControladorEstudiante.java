/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rob.cursojava8.behavior;

import com.rob.cursojava8.modelo.Estudiante;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Rob
 */
public class ControladorEstudiante {

    /**
     * Filtrado tradicional
     *
     * @param estudiantes
     * @return
     */
    public static List<Estudiante> filtrarPorEdad(List<Estudiante> estudiantes) {
        List<Estudiante> estudiantesRes = new ArrayList<>();
        for (Estudiante estudiante : estudiantes) {
            if (estudiante.getEdad() > 18) {
                estudiantesRes.add(estudiante);
            }
        }
        return estudiantesRes;
    }
    
    public static List<Estudiante> filtrarPorEdadNombre(List<Estudiante> estudiantes) {
        List<Estudiante> estudiantesRes = new ArrayList<>();
        for (Estudiante estudiante : estudiantes) {
            if (estudiante.getEdad() > 18 && estudiante.getNombre().startsWith("A")) {
                estudiantesRes.add(estudiante);
            }
        }
        return estudiantesRes;
    }
    
}
