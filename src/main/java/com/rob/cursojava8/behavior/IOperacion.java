/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rob.cursojava8.behavior;

/**
 *
 * @author Rob
 */
public interface IOperacion {

    public Integer operacion(int a, int b);
}
